<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{

  static public function getNews()
  {

    $news = News::where([['status', 1], ['type', 'news']]);

    return $news;

  }

  static public function getEvents()
  {

    $news = News::where([['status', 1], ['type', 'events']]);

    return $news;

  }

  static public function getNewsAndEventsForHome()
  {

    $news = News::where([['status', 1], ['to_home', 1]])->orderBy('date', 'desc');

    return $news;

  }

  static public function getItem($id)
  {

    $news = News::where([['status', 1], ['id', $id]])->get();

    return $news;

  }

}
