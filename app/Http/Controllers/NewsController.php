<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use View;

class NewsController extends Controller
{
	public function newsIndex($id = null){

		if($id != null){

			$item = News::getItem($id)->first();

			return view('pages.item', compact('item'));
		}

		$news = News::getNews()->paginate(3);

		return view('pages.news', compact('news'));
	}

	public function eventsIndex($id = null){

		if($id != null){

			$item = News::getItem($id)->first();

			return view('pages.item', compact('item'));
		}

		$events = News::getEvents()->paginate(3);

		return view('pages.events', compact('events'));
	}

}
