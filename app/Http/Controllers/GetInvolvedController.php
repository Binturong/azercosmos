<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class GetInvolvedController extends Controller
{
	public function index(){

		return view('pages.get_involved');
	}

}
