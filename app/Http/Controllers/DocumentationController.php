<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class DocumentationController extends Controller
{
	public function index(){

		return view('pages.documentation');
	}

}
