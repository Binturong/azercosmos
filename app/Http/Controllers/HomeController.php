<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;
use App\News;
use View;

class HomeController extends Controller
{
	public function index()
	{
		$news = News::getNewsAndEventsForHome()->paginate(5);

		// return view('pages.news', compact('news'));

		return view('pages.home', compact('news'));
	}

	// public function menu($slug = null, Request $request)
	// {
	// 	if (!$slug) {
	// 		return redirect()->action('HomeController@index');
	// 	}
  //
	// 	$menuData = Menu::where([['menus.slug', $slug], ['status', 1]])->first();
  //
	// 	return \App::call('App\Http\Controllers\\'.$slug.'Controller@index');
	// }

}
