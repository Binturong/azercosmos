<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

  static public function getMenuArray()
  {
    $menus = Menu::where('status', 1)->orderBy('sort_order')->get();

    // $menus = [];
    // foreach ($parents as $parent) {
    //   $p = [];
    //   $p['id'] = $parent['id'];
    //   $p['slug'] = $parent['slug'];
    //   $p['menu_name'] = $parent->content('menu_name', app('locale')->id);
    //   $p['page_title'] = $parent->content('page_title', app('locale')->id);
    //   $menu['parent'] = $p;
    //   $menu['childrens'] = [];
    //   foreach ($parent->childrens as $children) {
    //     if ($children->status == 1) {
    //       $p = [];
    //       $p['id'] = $children['id'];
    //       $p['slug'] = $children['slug'];
    //       $p['menu_name'] = $children->content('menu_name', app('locale')->id);
    //       $p['page_title'] = $children->content('page_title', app('locale')->id);
    //
    //       $menu['childrens'][] = $p;
    //     }
    //   }

    //   $menus[] = $menu;
    // }

    return $menus;

  }
}
