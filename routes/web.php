<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('main-route');
Route::get('/home', 'HomeController@index')->name('home-route');

Route::get('/news/{id?}', 'NewsController@newsIndex')->name('news-route');
Route::get('/events/{id?}', 'NewsController@eventsIndex')->name('events-route');
Route::get('/get_involved', 'GetInvolvedController@index')->name('get_involved-route');
Route::get('/documentation', 'DocumentationController@index')->name('documentation-route');
Route::get('/gallery', 'GalleryController@index')->name('gallery-route');
Route::get('/contacts', 'ContactsController@index')->name('contacts-route');
