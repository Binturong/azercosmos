@extends('index')

@section('content')
  <div class="content container" style="margin: 20px;">
    <main id="main" class="site-main" role="main">



      <article id="post-3033" class="post-3033 page type-page status-publish hentry">
        <!-- Page/Post Single Image Disabled or No Image set in Post Thumbnail -->	<div class="entry-container">
        <header class="entry-header">
          <h1 class="entry-title">Регулярная лига</h1>
        </header><!-- .entry-header -->

        <div class="entry-content">
          <p><strong>Технические описания электроники.</strong><br>
            <span id="more-3033"></span></p>
            <h4>2017</h4>
            <p><strong><a href="" target="_blank">Радиомодуль<br>
            </a><a href="" target="_blank">Термометр<br>
            </a><a href="" target="_blank">Распиновка модуля датчиков</a></strong><br>
            <strong> <a href="" target="_blank">Схема питания</a></strong><br>
            <strong> <a href="" target="_blank">Акселерометр<br>
            </a><a href="" target="_blank">Датчик давления</a></strong></p>
            <h4>2014-2016</h4>
            <p><strong><a href="">Микроконтроллер Atmega128 (PDF)</a></strong><br>
              <strong><a href="">Датчик давления MPX5100 (PDF)</a></strong><br>
              <strong><a href="">Температурный датчик DS18B20 (PDF)</a></strong><br>
              <strong><a href="">Радио-модуль RXQ2-433 (PDF)</a></strong></p>
              <p><strong>Электрические схемы.</strong></p>
              <p><strong><a href="">Принципиальная электрическая схема радио-модуля RXQ2-433 (PDF)</a></strong><br>
                <strong><a href="">Принципиальная электрическая схема модуля датчиков (PDF)</a></strong><br>
                <strong><a href="">Принципиальная электрическая схема модуля микроконтроллера (PDF)</a></strong></p>
              </div><!-- .entry-content -->
            </div><!-- .entry-container -->
          </article><!-- #post-## -->


        </main>
      </div>
    @endsection
