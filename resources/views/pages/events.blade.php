@extends('index')

@section('content')
  <div class="content">
    @foreach ($events as $item)
      <div class="box1">
        <div class="box1_img">
            <img src="images/img1.jpg" alt="" />
        </div>
        <div class="data">
            <h2><a href="{{ route('news-route', $item['id']) }}">{{$item['title']}}</a></h2>
            <div class="spans">
              <span><a href="{{ route('news-route', $item['id']) }}">{{$item['date']}}</a></span>
              <span><a href="{{ route('news-route', $item['id']) }}">{{$item['author']}}</span>
            </div>
            <p>{{$item['content_short']}}</p>
            <a href="{{ route('news-route', $item['id']) }}">Читать дальше</a>
        </div>
      <div class="clear"></div>
      </div>
    @endforeach
    <!-- <div class="box1">
        <h2><a href="single.html">Making it look like readable English. Many desktop publishing packages and web page</a></h2>
        <span>By Kieth Deviec- 2 hours ago</span>
      <div class="box1_img">
          <img src="images/img1.jpg" alt="" />
      </div>
      <div class="data">
          <p>Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editorsLorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors</p>
          <a href="single.html">Continue reading >>></a>
      </div>
    <div class="clear"></div>
    </div> -->


  <div class="page_links">
    <div class="page_numbers">
      <ul>
        {{ $events->links()}}
      </ul>
    </div>
  <div class="clear"></div>
    <div class="page_bottom">
      <p>Back To : <a href="#">Top</a> |  <a href="#">Home</a></p>
    </div>
  </div>
  </div>
@endsection
