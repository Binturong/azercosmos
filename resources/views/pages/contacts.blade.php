@extends('index')

@section('content')
  <div class="content container" style="margin-bottom: 10px;">

    <h1>Contacts</h1>
    <h4>МГУ, Ломоносовский корпус</h4>
    <p>
      Россия, г. Москва, Ломоносовский проспект, 27, корпус № 1, <br>
      Лаборатория аэрокосмической инженерии МГУ, комната Г719.
    </p>
    <p>НИИЯФ МГУ:<br>
        Радченко Владимир Вячеславович.<br>
        <a href="" target="_blank" rel="noopener">vrad1950@yandex.ru</a><br>
        +7 (495) 939-37-44<br>
        +7 (903) 251-94-09
    </p>
    <p>Технический директор проекта:<br>
        Веденькин Николай Николаевич.<br>
        <a href="" target="_blank" rel="noopener">vnn.space@gmail.com</a><br>
        +7&nbsp;926 218 88 97
    </p>
    <p>Координатор проекта:<br>
        Грачева Нина Алексеевна.<br>
        <a href="">eldin6725@gmail.com<br>
        </a>+7&nbsp;985&nbsp;723 79 84
    </p>
    <p>Наш канал в <a href="">Telegram</a><br>
        Вконтакте: <a href="">«CanSat в России»</a><br>
        Facebook: <a href="">«CanSat в России»</a>
    </p>
  </div>
@endsection
