@extends('index')

@section('content')
  <div class="content" style="min-height: 364px;">
    <div class="box1">
      <h2 style=" margin-bottom: 10px;"><a style="font-size: 28px;" href="{{ route('news-route', $item['id']) }}">{{$item['title']}}</a></h2>
      <div>
        <span><a href="{{ route('news-route', $item['id']) }}">{{$item['date']}}</a></span>
        <span><a href="{{ route('news-route', $item['id']) }}">{{$item['author']}}</a></span>
      </div>
      {{-- <div> --}}
        <div style="display: inline-block; margin: 10px; width: 300px; height: 200px;">
            <img src="/azercosmos/public/images/img4.jpg" alt="Fjords" width="300" height="200">
        </div>

        <div style="padding: 10px; display: inline-block; vertical-align: top; width: 65%;">
          {{$item['content']}}
          {{-- <a href="#">Continue reading >>></a> --}}
        </div>
        <div >
          @if ($item['type'] == 'news')
            <a href="{{ route('news-route') }}">Новости</a>
          @else
            <a href="{{ route('events-route') }}">События</a>
          @endif

        </div>
      {{-- </div> --}}
    </div>

  </div>
  @endsection
