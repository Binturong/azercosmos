<div class="header">
	<div class="header_bottom">
		<div class="wrap">
			<div class="menu">
			    <ul>
					@foreach ($menuItems as $menu)
							<li <?=(Request::segment(1) == $menu['slug'] ? 'class="current_page_item"' : '')?>>
									<a href="{{ route($menu['slug'].'-route') }}">{{ $menu['name'] }}</a>
							</li>
					@endforeach
			    </ul>
			</div>
			<!-- <div class="search_box">
			    <form>
			    <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}"><input type="submit" value="">
			    </form>
			</div> -->
			<div class="clear"></div>
		</div>
	</div>
</div>
