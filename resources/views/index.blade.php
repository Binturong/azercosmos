<!DOCTYPE HTML>
<html>
<head>
<title>The Free Blog Master Website Template | Home :: w3layouts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

{{--  Slick Slider  --}}
<link rel="stylesheet" href="{{ URL::asset('css/slick-slider/slick.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/slick-slider/slick-theme.css') }}" />
{{--  ///////////  --}}
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

 <link  href="../resources/assets/js/fancybox-master/dist/jquery.fancybox.min.css" rel="stylesheet">

<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" />
</head>
<body>

@include ('layouts.header')

<div class="wrap">
	<div class="main">

		@yield('content')

		{{-- @include ('layouts.sidebar') --}}


	<div class="clear"></div>
	</div>
</div>

@include ('layouts.footer')
<script src="{{ URL::asset('js/jquery-3.2.1.min.js') }}"></script>
{{--  Slick Slider  --}}
<script src="{{ URL::asset('js/slick-slider/slick.min.js') }}"></script>

<script src="{{ URL::asset('js/custom.js') }}"></script>
{{--  //////////  --}}

<script src="../resources/assets/js/fancybox-master/dist/jquery.fancybox.min.js"></script>
</body>
</html>
