<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>The Free Blog Master Website Template | contact :: w3layouts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<link href='//fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="header">
		<div class="header_top">
			<div class="wrap">
			     <div class="logo">
			       		<a href="index.html"><img src="images/logo.png" alt="" /></a>
			     </div>
			     <div class="login_button">
			    	<ul>
			    		<li><a href="#">Sign in</a><li> | 
			    		<li><a href="#">Login</a></li>
			   		</ul>	
			    </div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="header_bottom">
			<div class="wrap">
				<div class="menu">
				    <ul>
				    	<li><a href="index.html">HOME</a></li>
				    	<li><a href="single.html">ARTICLES</a></li>
				    	<li><a href="single.html">SERVICES</a></li>
				    	<li><a href="#">LOGOS</a></li>
				    	<li><a href="single.html">TOOLS</a></li>
				    	<li><a href="single.html">ICONS</a></li>
				    	<li><a href="single.html">WALLPAPERS</a></li>
				    	<li><a href="index.html">HELP</a></li>
				    	<li><a href="contact.html">CONTACT</a></li>
				    </ul>
				</div>
				<div class="search_box">
					    <form>
					    	<input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}"><input type="submit" value="">
					    </form>
				</div>
				<div class="clear"></div>
			</div>
		</div>
</div>
<div class="wrap">
		<div class="feed">
			<div class="feedback">
			   <h1>FEEDBACK</h1>
			   <form>
				   <label>NAME</label>
				   <input type="text" value="" id="name">
				   <label>E-MAIL</label>
				   <input type="text" value="">
				   <label>SUBJECT</label>
				   <textarea> </textarea><br>
				   <input type="submit" value="Submit">
			   </form>
			</div>
			<div class="map">
			<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d186905.27629471786!2d-81.24861995!3d42.948881!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882ef20ea88d9b0b%3A0x28c7d7699a056b95!2sLondon%2C+ON%2C+Canada!5e0!3m2!1sen!2sin!4v1427891904803"></iframe>

			</div>
		<div class="clear"></div>
		</div>
</div>
	<div class="footer">
		<div class="wrap">
			<div class="footer_grid1">
			   <h3>About Us</h3>
			   <p>Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here desktop publishing making it look like readable English.<br><a href="#">Read more....</a></p>
			</div>
			<div class="footer_grid2">
			   <h3>Navigate</h3>
					<div class="f_menu">
					   <ul>
					     <li><a href="index.html">Home</a></li>
					     <li><a href="single.html">Articles</a></li>
					     <li><a href="contact.html">Contact</a></li>
					     <li><a href="#">Write for Us</a></li>
					     <li><a href="#">Submit Tips</a></li>
					     <li><a href="#">Privacy Policy</a></li>
					   </ul>
					</div>
			</div>
		<div class="footer_grid3">
			<h3>We're Social</h3>
			<div class="img_list">
			   <ul>
			     <li><img src="images/facebook.png" alt="" /><a href="#">Facebook</a></li>
			     <li><img src="images/flickr.png" alt="" /><a href="#">Flickr</a></li>
			     <li><img src="images/google.png" alt="" /><a href="#">Google</a></li>
			     <li><img src="images/yahoo.png" alt="" /><a href="#">Yahoo</a></li>
			     <li><img src="images/youtube.png" alt="" /><a href="#">Youtube</a></li>
			     <li><img src="images/twitter.png" alt="" /><a href="#">Twitter</a></li>
			     <li><img src="images/yelp.png" alt="" /><a href="#">Help</a></li>
			   </ul>
			</div>
		</div>
		</div>
	<div class="clear"></div>
	</div>
<div class="f_bottom">
		<p>© 2012 Blog Master . All Rights Reserved | Design by<a href="http://w3layouts.com/"> W3Layouts</a></p>
</div>
</body>
</html>

