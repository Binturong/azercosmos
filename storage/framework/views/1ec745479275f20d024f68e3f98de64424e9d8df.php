<div class="header">
	<div class="header_bottom">
		<div class="wrap">
			<div class="menu">
			    <ul>
					<?php $__currentLoopData = $menuItems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<li <?=(Request::segment(1) == $menu['slug'] ? 'class="current_page_item"' : '')?>>
									<a href="<?php echo e(route($menu['slug'].'-route')); ?>"><?php echo e($menu['name']); ?></a>
							</li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			    </ul>
			</div>
			<!-- <div class="search_box">
			    <form>
			    <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}"><input type="submit" value="">
			    </form>
			</div> -->
			<div class="clear"></div>
		</div>
	</div>
</div>
