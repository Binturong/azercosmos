<?php $__env->startSection('content'); ?>
  <style>
    .wrap{ width: 80%;}
  </style>
  <div class="content">

    <div class="gallery-image">
      <a target="_blank"  data-fancybox="gallery" href="/azercosmos/public/images/img4.jpg">
        <img src="/azercosmos/public/images/img4.jpg" alt="Fjords" width="400" height="300">
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" href="/azercosmos/public/images/img5.jpg" data-fancybox="gallery">
        <img src="/azercosmos/public/images/img5.jpg" alt="Forest" >
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" data-fancybox="gallery" href="/azercosmos/public/images/img6.jpg">
        <img src="/azercosmos/public/images/img6.jpg" alt="Northern Lights" width="400" height="300">
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" data-fancybox="gallery" href="/azercosmos/public/images/img7.jpg">
        <img src="/azercosmos/public/images/img7.jpg" alt="Mountains" width="400" height="300">
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" data-fancybox="gallery" href="/azercosmos/public/images/img8.jpg">
        <img src="/azercosmos/public/images/img8.jpg" alt="Mountains" width="400" height="300">
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" data-fancybox="gallery" href="/azercosmos/public/images/img9.jpg">
        <img src="/azercosmos/public/images/img9.jpg" alt="Mountains" width="400" height="300">
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" data-fancybox="gallery" href="/azercosmos/public/images/img10.jpg">
        <img src="/azercosmos/public/images/img10.jpg" alt="Mountains" width="400" height="300">
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" data-fancybox="gallery" href="/azercosmos/public/images/img11.jpg">
        <img src="/azercosmos/public/images/img11.jpg" alt="Mountains" width="400" height="300">
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" data-fancybox="gallery" href="/azercosmos/public/images/img12.jpg">
        <img src="/azercosmos/public/images/img12.jpg" alt="Mountains" width="400" height="300">
      </a>
    </div>

  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>