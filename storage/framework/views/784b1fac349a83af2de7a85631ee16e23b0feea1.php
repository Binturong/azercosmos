<!DOCTYPE HTML>
<html>
<head>
<title>The Free Blog Master Website Template | Home :: w3layouts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo e(URL::asset('css/style.css')); ?>" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

<script src="../resources/assets/js/jquery-3.2.1.min.js"></script>

<link  href="../resources/assets/fancybox-master/dist/jquery.fancybox.min.css" rel="stylesheet">
<script src="../resources/assets/fancybox-master/dist/jquery.fancybox.min.js"></script>


</head>
<body>

<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="wrap">
	<div class="main">

		<?php echo $__env->yieldContent('content'); ?>

		<!-- <?php echo $__env->make('layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> -->


	<div class="clear"></div>
	</div>
</div>

<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</body>
</html>
