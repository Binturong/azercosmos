<?php $__env->startSection('content'); ?>
  <div class="content">

    <div class="gallery-image">
      <a target="_blank"  data-fancybox="gallery" href="/azercosmos/public/images/img1.jpg">
        <img src="/azercosmos/public/images/img1.jpg" alt="Fjords" width="300" height="200">
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" href="/azercosmos/public/images/img2.jpg" data-fancybox="gallery">
        <img src="/azercosmos/public/images/img2.jpg" alt="Forest" >
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" data-fancybox="gallery" href="/azercosmos/public/images/img3.jpg">
        <img src="/azercosmos/public/images/img3.jpg" alt="Northern Lights" width="300" height="200">
      </a>
    </div>

    <div class="gallery-image">
      <a target="_blank" data-fancybox="gallery" href="/azercosmos/public/images/img2_630X330.jpg">
        <img src="/azercosmos/public/images/img2_630X330.jpg" alt="Mountains" width="300" height="200">
      </a>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>