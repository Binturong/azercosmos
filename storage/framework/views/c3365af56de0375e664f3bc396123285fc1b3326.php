<?php $__env->startSection('content'); ?>
  <div class="content">
    <div class="three-pages-box">
      <div class="box1 three-pages">
        <div class="box1_img">
            <img src="images/img1.jpg" alt="" />
        </div>
        <div class="data">
            <h2><a href="single.html">Как стать участником</a></h2>
            <p>Приглашаем школьников 6-11 классов и студентов 1-2 курсов ВУЗов принять участие в чемпионате “Воздушно-инженерной школы МГУ” 2017-2018 года.</p>
        </div>
        <div class="clear"></div>
      </div>
      <div class="box1 three-pages">
        <div class="box1_img">
            <img src="images/img1.jpg" alt="" />
        </div>
        <div class="data">
            <h2><a href="single.html">Положение 7-го российского чемпионата проекта «Воздушно-инженерная школа»</a></h2>
            <p>К участию в чемпионате приглашаются команды школьников  6-11 классов, а также  студентов младших курсов вузов.</p>
        </div>
        <div class="clear"></div>
      </div>
      <div class="box1 three-pages">
        <div class="box1_img">
            <img src="images/img1.jpg" alt="" />
        </div>
        <div class="data">
            <h2><a href="single.html">Техническая документация</a></h2>
            <p>Технические описания и документы к заданиям.</p>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class="box1">
        <div class="box1_img">
            <img src="images/img1.jpg" alt="" />
        </div>
        <div class="data">
            <h2><a href="single.html"><?php echo e($item['title']); ?></a></h2>
            <span>By <?php echo e($item['author']); ?> - 2 hours ago</span>
            <p><?php echo e($item['content_short']); ?></p>
            <a href="<?php echo e(route($item['type'].'-route', $item['id'])); ?>">Continue reading >>></a>
        </div>
      <div class="clear"></div>
      </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <div class="box1">
      <div class="box1_img">
          <img src="images/img1.jpg" alt="" />
      </div>
      <div class="data">
          <h2><a href="single.html">Making it look like readable English.</a></h2>
          <span>By Kieth Deviec- 2 hours ago</span>
          <p>Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editorsLorem.</p>
          <a href="single.html">Continue reading >>></a>
      </div>
      <div class="clear"></div>
    </div>

    
  <div class="page_links">
    <div class="next_button">
       <a href="#">Next</a>
    </div>
    <div class="page_numbers">
        <ul>
      <li><a href="#">1</a>
      <li><a href="#">2</a>
      <li><a href="#">3</a>
      <li><a href="#">4</a>
      <li><a href="#">5</a>
      <li><a href="#">6</a>
      <li><a href="#">... Next</a>
      </ul>
    </div>
  <div class="clear"></div>
    <div class="page_bottom">
      <p>Back To : <a href="#">Top</a> |  <a href="#">Home</a></p>
    </div>
  </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>