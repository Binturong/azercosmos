<?php $__env->startSection('content'); ?>
  <div class="content" style="min-height: 364px;">
    <div class="box1">
      <h2 style=" margin-bottom: 10px;"><a style="font-size: 28px;" href="<?php echo e(route('news-route', $item['id'])); ?>"><?php echo e($item['title']); ?></a></h2>
      <div>
        <span><a href="<?php echo e(route('news-route', $item['id'])); ?>"><?php echo e($item['date']); ?></a></span>
        <span><a href="<?php echo e(route('news-route', $item['id'])); ?>"><?php echo e($item['author']); ?></a></span>
      </div>
      
        <div style="display: inline-block; margin: 10px; width: 300px; height: 200px;">
            <img src="/azercosmos/public/images/img4.jpg" alt="Fjords" width="300" height="200">
        </div>

        <div style="padding: 10px; display: inline-block; vertical-align: top; width: 65%;">
          <?php echo e($item['content']); ?>

          
        </div>
        <div >
          <?php if($item['type'] == 'news'): ?>
            <a href="<?php echo e(route('news-route')); ?>">Новости</a>
          <?php else: ?>
            <a href="<?php echo e(route('events-route')); ?>">События</a>
          <?php endif; ?>

        </div>
      
    </div>

  </div>
  <?php $__env->stopSection(); ?>

<?php echo $__env->make('index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>