<!DOCTYPE HTML>
<html>
<head>
<title>The Free Blog Master Website Template | Home :: w3layouts</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<link rel="stylesheet" href="<?php echo e(URL::asset('css/slick-slider/slick.css')); ?>" />
<link rel="stylesheet" href="<?php echo e(URL::asset('css/slick-slider/slick-theme.css')); ?>" />

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

 <link  href="../resources/assets/js/fancybox-master/dist/jquery.fancybox.min.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo e(URL::asset('css/app.css')); ?>" />
<link rel="stylesheet" href="<?php echo e(URL::asset('css/style.css')); ?>" />
</head>
<body>

<?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="wrap">
	<div class="main">

		<?php echo $__env->yieldContent('content'); ?>

		


	<div class="clear"></div>
	</div>
</div>

<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script src="<?php echo e(URL::asset('js/jquery-3.2.1.min.js')); ?>"></script>

<script src="<?php echo e(URL::asset('js/slick-slider/slick.min.js')); ?>"></script>

<script src="<?php echo e(URL::asset('js/custom.js')); ?>"></script>


<script src="../resources/assets/js/fancybox-master/dist/jquery.fancybox.min.js"></script>
</body>
</html>
