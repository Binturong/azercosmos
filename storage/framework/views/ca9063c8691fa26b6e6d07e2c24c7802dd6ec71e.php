<?php $__env->startSection('content'); ?>
  <div class="content">
    <div class="box1">
       <h2><a href="single.html"><?php echo e($item['title']); ?></a></h2>
       <span>By <?php echo e($item['author']); ?> - 2 hours ago</span>
       <p><?php echo e($item['content']); ?></p>
      <div class="top_img">
         <img src="images/img2_630X330.jpg" alt="" />
      </div>
      <div class="data_desc">
          <p><?php echo e($item['content']); ?></p>
          
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>