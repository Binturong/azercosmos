<div class="footer">
	<div class="wrap">
		<div class="footer_grid1">
			<h3>О нас</h3>
			<p>Целью проекта является привлечение школьной и студенческой молодёжи к наукоёмким и высокотехнологичным областям деятельности, прежде всего, к космическим исследованиям, и смежным разделам науки, индустрии и информационных технологий.
Организаторы проекта в России – НИИЯФ МГУ и Лаборатория Аэрокосмической Инженерии МГУ.</p>
		</div>
		<div class="footer_grid2">
			<h3>Навигация</h3>
				<div class="f_menu">
					<ul>
				       <li><a href="<?php echo e(route('home-route')); ?>">Главная</a></li>
				       <li><a href="<?php echo e(route('news-route')); ?>">Новости</a></li>
				       <li><a href="<?php echo e(route('events-route')); ?>">События</a></li>
				       <li><a href="<?php echo e(route('get_involved-route')); ?>">Как стать участником</a></li>
				       <li><a href="<?php echo e(route('documentation-route')); ?>">Техническая документация</a></li>
				       <li><a href="<?php echo e(route('gallery-route')); ?>">Галерея</a></li>
				       <li><a href="<?php echo e(route('contacts-route')); ?>">Контакты</a></li>
				   </ul>
				</div>
		</div>
	<div class="footer_grid3">
		<h3>Мы в соц. сетях</h3>
		<div class="img_list">
		    <ul>
		     <li><img src="/azercosmos/public/images/facebook.png" alt="" /><a href="#">Facebook</a></li>
		     <li><img src="/azercosmos/public/images/flickr.png" alt="" /><a href="#">Flickr</a></li>
		     <li><img src="/azercosmos/public/images/google.png" alt="" /><a href="#">Google</a></li>
		     <li><img src="/azercosmos/public/images/yahoo.png" alt="" /><a href="#">Yahoo</a></li>
		     <li><img src="/azercosmos/public/images/youtube.png" alt="" /><a href="#">Youtube</a></li>
		     <li><img src="/azercosmos/public/images/twitter.png" alt="" /><a href="#">Twitter</a></li>
		     <li><img src="/azercosmos/public/images/yelp.png" alt="" /><a href="#">Help</a></li>
		    </ul>
		</div>
	</div>
	</div>
<div class="clear"></div>
</div>
