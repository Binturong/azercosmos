$(document).ready(function(){
    // Slick slider
    $(".slider").slick({
        autoplay: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode:true,
        arrows: true,
        responsive: [{ 
            breakpoint: 480,
            settings: {
                dots: false,
                arrows: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode:true
            } 
        }]
    });   
    //////////////
})